/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jira.plugins.panels;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.web.bean.PagerFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.plugin.projectpanel.ProjectTabPanel;
import com.atlassian.jira.plugin.projectpanel.ProjectTabPanelModuleDescriptor;
import com.atlassian.jira.project.browse.BrowseContext;

import java.sql.Timestamp;
import java.util.*;

import org.joda.time.*;


public class TimeReportProjectTabPanel extends AbstractProjectTabPanel implements ProjectTabPanel
{
    private static final Logger log = LoggerFactory.getLogger(TimeReportProjectTabPanel.class);
    JiraDurationUtils jiraDurationUtils = ComponentAccessor.getComponent(JiraDurationUtils.class);
    private static SearchService searchService = ComponentAccessor.getComponentOfType(SearchService.class);

    protected Map<String, Object> createVelocityParams(BrowseContext context)
    {        
        Map<String, Object> params = super.createVelocityParams(context);
        Project project = context.getProject();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        UserManager userManager = ComponentAccessor.getUserManager();
        WorklogManager worklogManager = ComponentAccessor.getWorklogManager();
        HashMap<IssueType,Long> week = new HashMap<IssueType, Long>();
        HashMap<IssueType,Long> month = new HashMap<IssueType, Long>();

        HashMap<User, Long> userLoggedHoursWeek = new HashMap<User, Long>();
        HashMap<User, Long> userLoggedHoursMonth = new HashMap<User, Long>();

        User currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

        Timestamp weekDate = null;
        Timestamp monthDate = null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_WEEK, -7);
        weekDate = new Timestamp(cal.getTime().getTime());
        cal.add(Calendar.DAY_OF_WEEK, -30);
        monthDate = new Timestamp(cal.getTime().getTime());
        List<Issue> issues = null;
        Long totalHoursLoggedForWeek = 0L;
        Long totalHoursLoggedForMonth = 0L;

        try
        {

            String jqlQuery = "project = '"+project.getKey()+"' and updated > -4w and originalEstimate > 0";
            final SearchService.ParseResult parseResult = searchService.parseQuery(currentUser, jqlQuery);
            if (parseResult.isValid())
            {
                try
                {
                    final SearchResults results = searchService.search(currentUser,parseResult.getQuery(), PagerFilter.getUnlimitedFilter());
                    issues = results.getIssues();

                }
                catch (SearchException e)
                {
                    log.error("Error running search", e);
                }
            }
            else
            {
                log.error("Error parsing jqlQuery: " + parseResult.getErrors());
            }


            for(Issue issue: issueManager.getIssueObjects(issueManager.getIssueIdsForProject(project.getId())))
            {
                if(issue.getCreated().after(monthDate))
                {
                    Long timeSpent = (issue.getTimeSpent() != null) ? issue.getTimeSpent() : 0;


                    Collection<Worklog> worklogs = worklogManager.getByIssue(issue);

                    month.put(issue.getIssueTypeObject(), (month.get(issue.getIssueTypeObject()) != null) ? (Long)month.get(issue.getIssueTypeObject()) + timeSpent : timeSpent);

                    for(Worklog worklog:worklogs)
                    {
                        totalHoursLoggedForMonth += worklog.getTimeSpent();
                        if(userLoggedHoursMonth.containsKey(userManager.getUserObject(worklog.getAuthor())))
                        {
                            Long logged = userLoggedHoursMonth.get(userManager.getUserObject(worklog.getAuthor()));
                            userLoggedHoursMonth.put(userManager.getUserObject(worklog.getAuthor()),logged + worklog.getTimeSpent());
                        }
                        else
                        {
                            userLoggedHoursMonth.put(userManager.getUserObject(worklog.getAuthor()),worklog.getTimeSpent());
                        }
                    }

                    if(issue.getCreated().after(weekDate))
                    {
                        week.put(issue.getIssueTypeObject(), (week.get(issue.getIssueTypeObject()) != null) ? (Long)week.get(issue.getIssueTypeObject()) + timeSpent : timeSpent);

                        for(Worklog worklog:worklogs)
                        {
                            totalHoursLoggedForWeek += worklog.getTimeSpent();
                            if(userLoggedHoursWeek.containsKey(userManager.getUserObject(worklog.getAuthor())))
                            {
                                Long logged = userLoggedHoursWeek.get(userManager.getUserObject(worklog.getAuthor()));
                                userLoggedHoursWeek.put(userManager.getUserObject(worklog.getAuthor()),logged + worklog.getTimeSpent());
                            }
                            else
                            {
                                userLoggedHoursWeek.put(userManager.getUserObject(worklog.getAuthor()),worklog.getTimeSpent());
                            }
                        }
                    }
                }
            }

            params.put("issues",issues);

            params.put("week",week);
            params.put("month",month);

            params.put("userHoursWeek",userLoggedHoursWeek);
            params.put("userHoursMonth", userLoggedHoursMonth);

            params.put("totalHoursLoggedForWeek",totalHoursLoggedForWeek);
            params.put("totalHoursLoggedForMonth",totalHoursLoggedForMonth);

            params.put("avatarService",ComponentAccessor.getAvatarService());
            params.put("jiraDurationUtils",jiraDurationUtils);
        }
        catch(Exception exc)
        {
            log.error("Unable to display time report project panel. " + exc.toString());            
        }

        return params;
    }

    public boolean showPanel(BrowseContext context)
    {
        return true;
    }
}
